import React, { useEffect, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Service from './Service';
import useLocalStorage from './useLocalStorage';

export default function Join(props) {

  const { Invite } = props.match.params;

  const [, setGames] = useLocalStorage("games", []);
  const [game, setGame] = useState({});
  const [joined, setJoined] = useState({});

  const service = Service();

  useEffect(() => {
    const { Invite } = props.match.params;
    const service = Service();
    service.checkGame(Invite).then((game) => {
      setGame(game);
    });

  }, [props.match.params]);

  function joinGame() {
    service.joinGame(Invite).then(game => {
      setGames(gs => [...gs, game]);
      setJoined(game);
    });
  }

  if (joined.Id !== undefined && joined.Key !== undefined) {
    const path = `/game/${joined.Id}/${joined.Key}`;
    return (<Redirect to={path} />);
  }

  if (game === undefined) {
    return (<div>Loading...</div>);
  }
  const watch = `/game/${game.Id}/watch`;
  return (
    <div className="panel">
      <div className="row">
        <div>{game.Name}<br />{game.CreatedAt}</div>
        <div>{game.Length} pts</div>
      </div>
      <div className="text-center">
        <button onClick={joinGame}>Join</button>
        <br />
        or <Link to={watch}>Watch</Link>
      </div>
    </div>
  );
}
