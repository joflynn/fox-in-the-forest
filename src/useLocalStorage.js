import { useState } from 'react';

export default function useLocalStorage(name, init) {

  
  const [value, setValue] = useState(() => {
    try {
      const item = window.localStorage.getItem(name);
      return item ? JSON.parse(item) : init;
    } catch (error) {
      console.log(error);
      return init;
    }
  });

  function wrappedSetValue(newValue) {
    try {
      const updatedValue = (newValue instanceof Function) ? newValue(value) : newValue;
      setValue(updatedValue);
      window.localStorage.setItem(name, JSON.stringify(updatedValue));
    } catch (error) {
      console.log(error);
    }
  }

  return [value, wrappedSetValue];
}
