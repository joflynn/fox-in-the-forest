import React from 'react';

export default function Scores(props) {
  const { game } = props;
  const { Tricks, RoundScore, Score, Length } = game;

  return (
    <table>
      <thead>
        <tr>
          <th><label>Scores</label></th>
          <td>North</td>
          <td>South</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Tricks</td>
          <th>{Tricks.North}</th>
          <th>{Tricks.South}</th>
        </tr>
        <tr className="hide-closed">
          <td>Round</td>
          <th className="laurel">{RoundScore.North}</th>
          <th className="laurel">{RoundScore.South}</th>
        </tr>
        <tr>
          <td>Game ({Length})</td>
          <th className="laurel">{Score.North}</th>
          <th className="laurel">{Score.South}</th>
        </tr>
      </tbody>
    </table>
  );
}
