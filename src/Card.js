import React from 'react';

export default function Card(props) {

  const { card, selectedCard, newCard, onClick } = props;

  const Names = {
    1: "Swan",
    3: "Fox",
    5: "Woodcutter",
    7: "Treasure",
    9: "Witch",
    11: "Monarch"
  };

  const Descs = {
    1: "If you lose this trick, you lead the next",
    3: "May swap a card in hand with current Decree",
    5: "Draw a card, then discard a card",
    7: "Winner of trick gets a point",
    9: "If only one witch is played, it counts as a trump",
    11: "Opponent must either play 1, or highest card in suit, if possible"
  };

  if (card === undefined) {
    return (
      <div className="card missingCard">-</div>
    );
  }
  if (card === "?") {
    return (
      <div className="card hiddenCard">?</div>
    );
  }

  const Name = Names[card.Rank];
  const Desc = Descs[card.Rank];

  const selected = (selectedCard === undefined || selectedCard.Rank !== card.Rank || selectedCard.Suit !== card.Suit) ? "" : "selectedCard";
  const isNew = (newCard === undefined || newCard.Rank !== card.Rank || newCard.Suit !== card.Suit) ? "" : "newCard";
  const classNames = `card ${card.Suit} ${selected} ${isNew}`;
  return (
    <div className={`card-wrapper ${selected}`}>
      <div className={classNames} title={Desc} onClick={onClick}>
        <div className="rank">{card.Rank}</div>
        <div className="suit">{/*card.Suit*/}</div>
        <div className="name">{Name}</div>
      </div>
    </div>
  );
}
