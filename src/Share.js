import React, { useState } from 'react';
import config from './config';

export default function Share(props) {

  const [copied, setCopied] = useState("");
  const { game } = props;
  const { Invite } = game;
  const url = `${config.baseUrl}/join/${Invite}`;

  function copyAll(e) {
    e.target.select();
    document.execCommand('copy');
    e.target.focus();
    setCopied("copied.");

    setTimeout(() => setCopied(""), 5000);
  }

  return (
    <div className="panel">
      <div className="row">
        <div>{game.Name}</div>
        <div>{game.Length}pts</div>
      </div>
      <div>
        <p>Share this link with a friend to play.</p>
        <textarea rows="5" readOnly onClick={copyAll} value={url} />
        <div>{copied}</div>
      </div>
      <div className="text-center">
        <p>Waiting for second player.</p>
      </div>
    </div>
  );
}
