import shuffleSeed from 'shuffle-seed';

export default function Deck(seed) {
  const suits = ["B", "M", "K"];
  const cards = Array.from({length: 33}, (v, i) => [suits[Math.floor(i / 11)], i % 11 + 1]);
  console.log(cards);
  const deck = shuffleSeed.shuffle(cards, seed);
  console.log(deck);
}
