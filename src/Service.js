import config from './config';
export default function Service(props = {}) {

  const { endpoint } = config;

  const headers = {
    "Accept": "application/json",
    "Content-Type": "application/json"
  };

  const { Id, Key, setNotice } = props;

  function checkError(response) {
    if (response.status !== 200) {
      response.text().then(body =>{
        console.log(body);
        if (setNotice !== undefined) {
          setNotice(body);
        }
      });
      return Promise.reject();
    } else {
      return response;
    }
  }


  function createGame(Name, Length) {
    return fetch(endpoint + "/games/create", { method: "POST", headers, body: JSON.stringify({Name, Length}) })
      .then(checkError)
      .then(response => response.json());
  }

  function checkGame(invite) {
    return fetch(endpoint + `/games/check/${invite}`, { method: "GET", headers})
      .then(checkError)
      .then(response => response.json());
  }

  function joinGame(invite) {
    return fetch(endpoint + `/games/join/${invite}`, { method: "PUT", headers })
      .then(checkError)
      .then(response => response.json());
  }

  function status() {
    return fetch(endpoint + `/game/${Id}/${Key}?x=${Date.now()}`, { method: "GET", headers })
      .then(checkError)
      .then(response => response.json());
  }

  function play(Card) {
    return fetch(endpoint + `/game/${Id}/play`, { method: "POST", headers, body: JSON.stringify({Key, Card}) })
      .then(checkError)
      .then(response => response.json());
  }

  function fox(NewDecree) {
    return fetch(endpoint + `/game/${Id}/fox`, { method: "POST", headers, body: JSON.stringify({Key, NewDecree}) })
      .then(checkError)
      .then(response => response.json());
  }

  function woodcutter(Discard) {
    return fetch(endpoint + `/game/${Id}/woodcutter`, { method: "POST", headers, body: JSON.stringify({Key, Discard}) })
      .then(checkError)
      .then(response => response.json());
  }

  return Object.freeze({createGame, checkGame, joinGame, status, play, fox, woodcutter});
}
