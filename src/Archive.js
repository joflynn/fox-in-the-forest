import React from 'react';
import { Link } from 'react-router-dom';
import useLocalStorage from './useLocalStorage';
import GameCard from './GameCard';

export default function Archive() {

  const [archive] = useLocalStorage('archive', []);

  const gameCards = archive.reverse().map(game => {
    const { Id, Key } = game
    const path = `/game/${Id}/${Key}`;
    return (
      <Link to={path} key={Id}>
        <GameCard game={game} />
      </Link>
    );
  });

  return (
    <div>
      <h1 className="text-center">Archived</h1>
      <div className="game-list">
        <Link to="/"><div className="game new">Home</div></Link>
        {gameCards}
      </div>
    </div>
  );
}
