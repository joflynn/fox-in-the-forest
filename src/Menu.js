import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Scores from './Scores';

export default function Menu(props) {

  const { Name, Status, notice, game } = props;

  const [open, setOpen] = useState(false);

  const classes = `menu ${open ? "open" : "closed"}`;

  function toggleOpen() {
    setOpen(open => !open);
  }

  return (
    <div className={classes} onClick={toggleOpen}>
      <div>
        <div className="hide-closed"><label>Game Name: </label>{Name}</div>
        <div><label>Status: </label>{Status}</div>
      </div>
      <div className="notice">
        <label>Notice: </label>
        {notice}
      </div>
      <Scores game={game} />
      <div className="hide-closed">
        <button>Close</button>&nbsp;
        <Link to="/">Home</Link>
      </div>
    </div>
  );
}
