const dev = {
  endpoint: "http://localhost:4000/fitf",
  baseUrl: "http://localhost:3000/game"
};

const prod = {
  endpoint: "https://fitf-backend.herokuapp.com/fitf",
  baseUrl: "http://fitf.s3-website-us-west-2.amazonaws.com/game"
};

const config = process.env.NODE_ENV !== "development" ? prod : dev;

export default {
  ...config 
};
