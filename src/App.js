import React, { useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import './App.css';

import Home from './Home';
import Archive from './Archive';
import Create from './Create';
import Join from './Join';
import Game from './Game';

function App() {

  useEffect(() => {
    document.title = "FitF";
  }, []);

  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/archive" component={Archive} />
        <Route path="/game/create" component={Create} />
        <Route path="/game/join/:Invite" component={Join} />
        <Route path="/game/:Id/watch" component={Game} />
        <Route path="/game/:Id/:Key" component={Game} />
      </Switch>
    </Router>
  );
}

export default App;
