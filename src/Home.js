import React from 'react';
import { Link } from 'react-router-dom';
//import Service from './Service';
import useLocalStorage from './useLocalStorage';
import GameCard from './GameCard';

export default function Home() {

  const [games, setGames] = useLocalStorage('games', []);
  const [archive, setArchive] = useLocalStorage('archive', []);

  function makeArchive(id) {
    return (e) => {
      e.preventDefault();

      const idx = games.findIndex((game) => game.Id === id);
      setGames([ ...games.slice(0, idx), ...games.slice(idx + 1)]);
      setArchive([...archive, games[idx]]);
    }
  }

  const gameCards = games.reverse().map(game => {
    const path = `/game/${game.Id}/${game.Key}`;
    const { Id } = game;
    const archive = makeArchive(Id);
    return (
      <Link to={path} key={Id}>
        <GameCard game={game} onX={archive} />
      </Link>
    );
  });

  const archiveLink = archive.length ? <Link to="/archive"><div className="game old">Archived Games</div></Link> : "";

  return (
    <div>
      <h1 className="text-center">FitF</h1>
      <div className="game-list">
        <Link to="/game/create">
          <div className="game new">
            Create new game
          </div>
        </Link>
        {gameCards}
        {archiveLink}
      </div>
    </div>
  );
}
