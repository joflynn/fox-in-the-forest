import React from 'react';
import Card from './Card';
export default function Trick(props) {
  const { Decree, North, South } = props.Trick;

  let actualDecree = Decree;
  props.Trick.Fox.forEach(({NewDecree}) => { actualDecree = NewDecree});


  let lastTrick = "";
  if (props.LastTrick !== undefined) {
    lastTrick = (
      <div className="last-trick">
        <Trick Trick={props.LastTrick} />
      </div>
    );
  }
  return (
    <div className="trick">
      <div className="decree">
        <Card card={actualDecree} />
      </div>
      <div className="north">
        <Card card={North} />
      </div>
      <div className="south">
        <Card card={South} />
      </div>
      {lastTrick}
    </div>
  );
}
