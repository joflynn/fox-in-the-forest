import React, { useState } from 'react';
import Card from './Card';

export default function Hand(props) {
  const { Side, Hand, Turn, Decree, NewCard, onPlay } = props;

  const Active = (Side === Turn) ? "turn" : "";


  const [selectedCard, setSelectedCard] = useState(undefined);

  let onClick = (x) => {console.log(x)};
  if (Side === Turn) {
    onClick = (card) => {
      console.log("selecting", card);
      if (selectedCard === undefined) {
        return setSelectedCard(card);
      }
      if (card.Rank === selectedCard.Rank && card.Suit === selectedCard.Suit) {
        onPlay(card);
      } else {
        setSelectedCard(card);
      }
    }
  }

  const classNames = `hand ${Side} ${Active}`;

  Hand.sort((a, b) => {
    if (a.Suit === Decree.Suit && b.Suit !== Decree.Suit) {
      return -1;
    }
    if (b.Suit === Decree.Suit && a.Suit !== Decree.Suit) {
      return 1;
    }
    if (a.Suit !== b.Suit) {
      if (a.Suit < b.Suit) {
        return -1;
      }
      return 1;
    }
    if (a.Rank < b.Rank-0) {
      return -1;
    }
    return 1;
  });
  const cards = Hand.map((card, idx) => {
    const key = (card === "?") ? idx : `${card.Rank}${card.Suit}`;
    return (<Card key={key} card={card} selectedCard={selectedCard} newCard={NewCard} onClick={() => onClick(card)} />);
  });
  return (
    <div className={classNames}>
      {cards}
    </div>
  );
}
