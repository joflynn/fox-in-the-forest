import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import useLocalStorage from './useLocalStorage';
import Service from './Service';

export default function Create() {

  const service = Service();

  const [Name, setName] = useState(generateName());
  const [Length, setLength] = useState(25);
  const [Games, setGames] = useLocalStorage("games", []);
  const [redirect, setRedirect] = useState();

  function generateName() {
    const words = ["Monarch", "Witch", "Treasure", "Woodcutter", "Fox", "Swan", "Bells", "Keys", "Moons"];
    const choices = [];
    for (let i = 0; i < 4; i++) {
      const idx = Math.floor(Math.random() * words.length);
      choices.push(words[idx]);
    }
    return choices.join(" ");
  }

  function handleChange(event) {
    const { target } = event;
    const { name, value } = target;
    switch(name) {
      case "Name":
        setName(value);
        break;

      case "Length":
        setLength(value);
        break;

      default:
        break;
    }
  }

  function createGame() {
    service.createGame(Name, Length).then(game => {
      const { Id, Name, Key, Invite } = game;
      const CreatedAt = Date.now();
      setGames([...Games, { Id, Name, Key, Invite, CreatedAt }]);
      setRedirect(`/game/${Id}/${Key}`);
    });
  }

  if (redirect) {
    return <Redirect to={redirect} />;
  }

  return (
    <div>
      <h1 className="text-center">FitF</h1>
      <div className="panel">
        <div className="field">
          <label>Name</label>
          <input value={Name} name="Name" onChange={handleChange} />
        </div>
        <div className="field">
          <label>Length</label>
          <input type="range" name="Length" onChange={handleChange} list="lengths" value={Length} min="16" max="31"/>
          <datalist id="lengths">
            <option value="16" label="16 pts" />
            <option value="25" label="25 pts" />
            <option value="31" label="31 pts" />
          </datalist>
        </div>
        <div className="field">
          <button onClick={createGame}>Create</button>
        </div>
      </div>
    </div>
  );
}
