import React, { useEffect, useState, useCallback } from 'react';
import Service from './Service';
import Loading from './Loading';
import Share from './Share';
import Hand from './Hand';
import Trick from './Trick';
import Menu from './Menu';

export default function Game(props) {

  const UPDATE_INTERVAL = 15000;

  const { Id, Key } = props.match.params;

  const [game, setGame] = useState({});
  const [newCard, setNewCard] = useState();
  const [notice, setNotice] = useState("");

  const service = Service({ Id, Key, setNotice });

  const updateGame = useCallback(() => {
    const service = Service({ Id, Key, setNotice });
    service.status().then(game => {
      setGame(game);
      setNotice("");
    });
  }, [Id, Key]);

  useEffect(() => {
    updateGame();
    const timer = setInterval(updateGame, UPDATE_INTERVAL);
    return () => {
      clearInterval(timer);
    };
  }, [updateGame]);

  if (game.Status === "Setup") {
    if (Key === undefined) {
      return (<div className="panel"><div className="row">Waiting...</div></div>);
    }
    return (<Share game={game} />);
  }
  if (game.Status === undefined) {
    return (<Loading game={game} />);
  }


  const { Name, Status, Turn, Side } = game;

  const North = (Side === "North") ? game.Hand : Array.from({length: 13 + 1 - game.Trick.Index - ((game.Trick.North === undefined)?0:1)}, () => "?");
  const South = (Side === "South") ? game.Hand : Array.from({length: 13 + 1 - game.Trick.Index - ((game.Trick.South === undefined)?0:1)}, () => "?");



  function playCard(card) {
    switch (game.Special) {
      case "Fox":
        setNewCard(undefined);
        service.fox(card).then(response => updateGame());
        break;

      case "Woodcutter":
        setNewCard(undefined);
        service.woodcutter(card).then(response => updateGame());
        break;

      default: 
        service.play(card).then(response => {
          if (response.Status === "Woodcutter Draw") {
            setNewCard(response.Card);
          }
          if (response.status === "May Change Decree") {
            setNewCard(response.Decree);
          }
          updateGame()
        });
    }
  }

  const currentNotice = notice || game.Notice;
  return (
    <div className="game">
      <Menu Name={Name} Status={Status} notice={currentNotice} game={game} />
      <Hand Side="North" Hand={North} Turn={Turn} Decree={game.Trick.Decree} NewCard={newCard} onPlay={playCard} />
      <Trick Trick={game.Trick} LastTrick={game.LastTrick} />
      <Hand Side="South" Hand={South} Turn={Turn} Decree={game.Trick.Decree} NewCard={newCard} onPlay={playCard} />
    </div>
  );
}
