import React from 'react';

export default function GameCard(props) {
  const { game, onX } = props;
  const { Name, CreatedAt } = game;
  const CreatedAtDateTime = new Date(CreatedAt);

  const xButton = onX ? <button className="archive" onClick={onX}>x</button> : "";

  return (
    <div className="game">
      <div>{Name}</div>
      <div>Started {CreatedAtDateTime.toLocaleDateString()} {CreatedAtDateTime.toLocaleTimeString()}</div>
      {xButton}
    </div>
  );
}
